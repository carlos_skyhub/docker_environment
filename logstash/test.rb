require 'logstash-logger'

logger_tcp = LogStashLogger.new(type: :tcp, port: 5000 )
logger_udp = LogStashLogger.new(type: :udp, port: 5001 )

logger_tcp.info 'test'
logger_tcp.error '{"message": "error"}' 
logger_tcp.debug message: 'test', foo: 'bar' 
logger_tcp.warn LogStash::Event.new(message: 'test', foo: 'bar') 
logger_tcp.tagged('foo') { logger_tcp.fatal('bar') }


logger_udp.info 'test'
logger_udp.error '{"message": "error"}' 
logger_udp.debug message: 'test', foo: 'bar' 
logger_udp.warn LogStash::Event.new(message: 'test', foo: 'bar') 
logger_udp.tagged('foo') { logger_udp.fatal('bar') }
