# timeline

rabbitmqadmin declare queue name=timeline-dev durable=true
rabbitmqadmin declare queue name=timeline durable=true

rabbitmqadmin declare exchange name=timeline type=direct

rabbitmqadmin declare binding source=timeline destination_type=queue destination=timeline
rabbitmqadmin declare binding source=timeline destination_type=queue destination=timeline-dev

#logs
rabbitmqadmin declare queue name=logs durable=true
rabbitmqadmin declare queue name=workers.logs durable=true

rabbitmqadmin declare exchange name=logs type=direct
rabbitmqadmin declare exchange name=workers.logs type=direct

rabbitmqadmin declare binding source=logs destination_type=queue destination=logs
rabbitmqadmin declare binding source=workers.logs destination_type=queue destination=workers.logs

# order & product index

rabbitmqadmin declare queue name=order_indices3 durable=true
rabbitmqadmin declare queue name=product_indices durable=true

rabbitmqadmin declare exchange name=order_indices3 type=direct
rabbitmqadmin declare exchange name=product_indices type=direct

rabbitmqadmin declare binding source=order_indices3 destination_type=queue destination=order_indices3
rabbitmqadmin declare binding source=product_indices destination_type=queue destination=product_indices

# product action result

rabbitmqadmin declare queue name=product_action_results durable=true

rabbitmqadmin declare exchange name=product_action_results type=direct

rabbitmqadmin declare binding source=product_action_results destination_type=queue destination=product_action_results
