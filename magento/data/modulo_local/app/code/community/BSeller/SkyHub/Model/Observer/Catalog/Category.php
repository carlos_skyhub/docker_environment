<?php
/**
 * BSeller Platform | B2W - Companhia Digital
 *
 * Do not edit this file if you want to update this module for future new versions.
 *
 * @category  BSeller
 * @package   BSeller_SkyHub
 *
 * @copyright Copyright (c) 2018 B2W Digital - BSeller Platform. (http://www.bseller.com.br)
 *
 * @author    Tiago Sampaio <tiago.sampaio@e-smart.com.br>
 */

class BSeller_SkyHub_Model_Observer_Catalog_Category extends BSeller_SkyHub_Model_Observer_Abstract
{

    use BSeller_SkyHub_Model_Integrator_Catalog_Category_Validation;

    
    /**
     * @param Varien_Event_Observer $observer
     */
    public function integrateCategory(Varien_Event_Observer $observer)
    {
        if (!$this->canRun()) {
            return;
        }

        /** @var Mage_Catalog_Model_Category $category */
        $category = $observer->getData('category');

        if (!$this->canIntegrateCategory($category)) {
            return;
        }

        /** Create or Update Product */
        $this->catalogCategoryIntegrator()->createOrUpdate($category);
    }


    /**
     * @param Varien_Event_Observer $observer
     */
    public function deleteCategory(Varien_Event_Observer $observer)
    {
        if (!$this->canRun()) {
            return;
        }

        /** @var Mage_Catalog_Model_Category $category */
        $category = $observer->getData('category');

        if (!$this->canIntegrateCategory($category)) {
            return;
        }

        /** Create or Update Product */
        $this->catalogCategoryIntegrator()->delete($category->getId());
    }
}
