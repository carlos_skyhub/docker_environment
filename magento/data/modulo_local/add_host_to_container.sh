#!/bin/bash
HOST_IP=$(/sbin/ip route|awk '/default/ { print $3 }')
RES=$(grep api.skyhub.com.br /etc/hosts)

if [ "$RES" ]; then
  HOST_LINE="$HOST_IP api.skyhub.com.br"
  sed -i '' '/\api.skyhub.com.br/s/.*/$HOST_LINE/' /etc/hosts
else
  sh -c -e "echo '$HOST_IP api.skyhub.com.br' >> /etc/hosts";
fi
