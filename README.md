Ambiente de desenvolvimento contendo:

- Elasticsearch 2.3.0 /  Kibana 4.5 /Logstash 2.3.0 -> https://hub.docker.com/r/sebp/elk/
- Mongo 3.4       -> https://hub.docker.com/r/dubc/mongodb-3.4/
- Mongo 4.0       -> https://hub.docker.com/r/dubc/mongodb-4.0/
- Redis 4.0.9     -> https://hub.docker.com/_/redis/
- RabbitMQ 3.8-management-alpine -> https://hub.docker.com/_/rabbitmq/

Antes de prosseguir é necessário que o docker e o docker-compose estejam instalados e o serviço do docker esteja em execução.

Instalar docker: https://docs.docker.com/install/

Instalar docker-compose: https://docs.docker.com/compose/install/ 

Utilizar docker sem a necessidade de executar usando sudo: https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user 

Obs: Para executar os seguintes comandos é preciso estar no mesmo diretório que o arquivo docker-compose.yml


O comando abaixo irá baixar todas as imagens necessárias para rodar os containers, pode ser que demore um pouco.

$ docker-compose create

Existem algumas opções para iniciar os containers:

1 - Iniciar com 'up' levanta todos os containers e trava o terminal exibindo o log de cada container no terminal

$ docker-compose up

2 - Iniciar com 'start' ou 'up -d' levanta os containers e libera o controle do terminal para o usuário. 

$ docker-compose start
$ docker-compose up -d

Estes comandos aceitam como parametro o nome do container a ser levantado.

Ex.: Caso seja preciso rodar apenas o container redis

$ docker-compose up redis
$ docker-compose up -d redis
$ docker-compose start redis

Se for preciso visualisar os logs 

$ docker-compose logs
$ docker-compose logs redis

Para parar os containers

$ docker-compose stop // se o terminal estiver 'preso' aos logs será preciso um ctrl-c

Para acessar o shell de algum container

$ docker-compose exec CONTAINER_NAME bash

Ex.: 

$ docker-compose exec redis bash

$ docker-compose exec mongo bash


-- -------------------------------------------------------

Observações

Database

- Os bancos de dados terão seus dados armazenados na pasta 'data/'


-- -------------------------------------------------------


Kibana

Pode ser acessado pela url: http://localhost:5601/

Caso o Kibana obrigue a criação de um índice e não permita criar o índice logstash-*:

$ docker-compose exec elk bash

$ /opt/logstash/bin/logstash -e 'input { stdin { } } output { elasticsearch { hosts =>["localhost"] } }'

Quando aparecer o texto

""
Settings: Default pipeline workers: 4
Pipeline main started
""

Digitar qualquer coisa e apertar enter. Depois Ctrl-C

-- ------------------------------------------------------------

RabbitMQ

Manager: http://localhost:15672/
user / password : guest / guest

Acessar o container do rabbitmq para criar os exchanges e queues 

docker-compose exec rabbitmq bash

Copiar o conteudo do arquivo rabbitmq/configure.sh e rodar no shell


-- -----------------------------------------------------------

Logstash

0- Executar a etapa do RabbitMQ, caso contrário o logstash irá falhar por não encontrar as filas em uso nos arquivos de configuração
1- Acessar a máquina elk e remover os arquivos de configuração que estão na pasta /etc/logstash/conf.d/
Adicionar um novo arquivo nesta pasta chamado 00-skyhub.conf com o conteudo abaixo:

input {
  tcp {
    port => 5000
    type => syslog
  }
  udp {
    port => 5001
    type => syslog
  }
}

output {
  elasticsearch {
    hosts => ["localhost"]
    sniffing => true
    manage_template => false
    index => "logs-%{+YYYY.MM.dd}"
    document_type => "%{[@metadata][type]}"
}

2- Mapear as portas 5000 e 5001 no docker

3- Reiniciar o elk

4- Adicionar no kibana o indice logs-*

5- Para testar:
5.1- instalar a gem do logstash (gem install logstash-logger) 
5.2- criar um arquivo logger.rb com o seguinte conteudo

require 'logstash-logger'

logger = LogStashLogger.new(type: :tcp, port: 5000 )
logger.info 'test'
logger.error '{"message": "error"}'
logger.debug message: 'test', foo: 'bar'
logger.warn LogStash::Event.new(message: 'test', foo: 'bar')
logger.tagged('foo') { logger.fatal('bar') }

5.3- executar: ruby logger.rb

6- Checar no kibana se os logs foram criados


-- -----------------------------------------------------------

Konga

Para baixar a imagem do ECR

eval $(aws ecr get-login --region us-east-2 --no-include-email)

-- -----------------------------------------------------------
LDAP

Para adicionar dados para teste

ldapadd  -D "cn=admin,dc=example,dc=org" -w admin -f ldap/test_data.ldif